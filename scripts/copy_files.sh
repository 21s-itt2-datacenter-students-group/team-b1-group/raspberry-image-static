#!/usr/bin/env bash

echo "copying extra files"

MNTDIR="mnt"

cp templates/ifcfg-interface-eth0 $MNTDIR/etc/network/interfaces.d/eth0


echo "Creating folder and copying from local files"

# Making a directory and adding the document to it
mkdir $MNTDIR/home/pi/Documents
cp -v python_code/dht11_adafruit.py $MNTDIR/home/pi/Documents/dht11_adafruit.py


#echo "Cloning gitlab repo to image"
#mkdir $MNTDIR/home/pi/Documents/team-b1
#git clone git@gitlab.com:21s-itt2-datacenter-students-group/team-b1.git
#cp -r -v Team B1 $MNTDIR/home/pi/Documents/team-b1
